﻿namespace JetChargeConfigApp
{
    partial class ConfigAppMainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigAppMainWindow));
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.cboPorts = new System.Windows.Forms.ComboBox();
            this.grpSerialPortSetup = new System.Windows.Forms.GroupBox();
            this.btnOpenSerialPort = new System.Windows.Forms.Button();
            this.txtBaudRate = new System.Windows.Forms.TextBox();
            this.txtComPort = new System.Windows.Forms.TextBox();
            this.cboBaudRate = new System.Windows.Forms.ComboBox();
            this.rtbIncomingData = new System.Windows.Forms.RichTextBox();
            this.txtRxDataWindow = new System.Windows.Forms.TextBox();
            this.btnClearTextBox = new System.Windows.Forms.Button();
            this.grpWiFiSetup = new System.Windows.Forms.GroupBox();
            this.btnClearWifiSettings = new System.Windows.Forms.Button();
            this.btnGenerateWebSocketKey = new System.Windows.Forms.Button();
            this.txtWebSocketKey = new System.Windows.Forms.TextBox();
            this.txtWebSocketKeyText = new System.Windows.Forms.TextBox();
            this.btnWiFiGetConfiguration = new System.Windows.Forms.Button();
            this.txtWiFiAccessPoint = new System.Windows.Forms.TextBox();
            this.btnWiFiSetConfiguration = new System.Windows.Forms.Button();
            this.txtWebSocketChannel = new System.Windows.Forms.TextBox();
            this.txtWebSocketPort = new System.Windows.Forms.TextBox();
            this.txtWebSocketUrl = new System.Windows.Forms.TextBox();
            this.txtServerUrl = new System.Windows.Forms.TextBox();
            this.txtWiFiPassword = new System.Windows.Forms.TextBox();
            this.txtWebSocketPortText = new System.Windows.Forms.TextBox();
            this.txtWebSocketChannelText = new System.Windows.Forms.TextBox();
            this.txtWebSocketUrlText = new System.Windows.Forms.TextBox();
            this.txtWiFiServerUrlText = new System.Windows.Forms.TextBox();
            this.txtWiFiPasswordText = new System.Windows.Forms.TextBox();
            this.txtWiFiAccessPointText = new System.Windows.Forms.TextBox();
            this.txtChargerIdText_1 = new System.Windows.Forms.TextBox();
            this.txtChargerId_1 = new System.Windows.Forms.TextBox();
            this.grpChargeStationConfig = new System.Windows.Forms.GroupBox();
            this.cboChargerIdPorts_1 = new System.Windows.Forms.ComboBox();
            this.txtChargerIdPortsText_1 = new System.Windows.Forms.TextBox();
            this.btnChargeStationSetConfig = new System.Windows.Forms.Button();
            this.btnChargeStationGetConfig = new System.Windows.Forms.Button();
            this.grpBoardInfo = new System.Windows.Forms.GroupBox();
            this.btnGetBoardInfo = new System.Windows.Forms.Button();
            this.txtFirmwareVersion = new System.Windows.Forms.TextBox();
            this.txtProductType = new System.Windows.Forms.TextBox();
            this.txtFirmwareVersionText = new System.Windows.Forms.TextBox();
            this.txtProductTypeText = new System.Windows.Forms.TextBox();
            this.grpSerialPortSetup.SuspendLayout();
            this.grpWiFiSetup.SuspendLayout();
            this.grpChargeStationConfig.SuspendLayout();
            this.grpBoardInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // serialPort
            // 
            this.serialPort.BaudRate = 115200;
            this.serialPort.PortName = "COM5";
            this.serialPort.ReadBufferSize = 8192;
            this.serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.ComPortDataReceive);
            // 
            // cboPorts
            // 
            this.cboPorts.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboPorts.FormattingEnabled = true;
            this.cboPorts.Location = new System.Drawing.Point(4, 39);
            this.cboPorts.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cboPorts.Name = "cboPorts";
            this.cboPorts.Size = new System.Drawing.Size(92, 22);
            this.cboPorts.TabIndex = 0;
            this.cboPorts.DropDown += new System.EventHandler(this.cboPorts_DropDown);
            // 
            // grpSerialPortSetup
            // 
            this.grpSerialPortSetup.Controls.Add(this.btnOpenSerialPort);
            this.grpSerialPortSetup.Controls.Add(this.txtBaudRate);
            this.grpSerialPortSetup.Controls.Add(this.txtComPort);
            this.grpSerialPortSetup.Controls.Add(this.cboBaudRate);
            this.grpSerialPortSetup.Controls.Add(this.cboPorts);
            this.grpSerialPortSetup.Font = new System.Drawing.Font("Calibri", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpSerialPortSetup.Location = new System.Drawing.Point(9, 10);
            this.grpSerialPortSetup.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpSerialPortSetup.Name = "grpSerialPortSetup";
            this.grpSerialPortSetup.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.grpSerialPortSetup.Size = new System.Drawing.Size(196, 102);
            this.grpSerialPortSetup.TabIndex = 1;
            this.grpSerialPortSetup.TabStop = false;
            this.grpSerialPortSetup.Text = "Serial Port Setup";
            // 
            // btnOpenSerialPort
            // 
            this.btnOpenSerialPort.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenSerialPort.Location = new System.Drawing.Point(51, 65);
            this.btnOpenSerialPort.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOpenSerialPort.Name = "btnOpenSerialPort";
            this.btnOpenSerialPort.Size = new System.Drawing.Size(89, 32);
            this.btnOpenSerialPort.TabIndex = 4;
            this.btnOpenSerialPort.Text = "Open Port";
            this.btnOpenSerialPort.UseVisualStyleBackColor = true;
            this.btnOpenSerialPort.Click += new System.EventHandler(this.btnOpenSerialPort_Click);
            // 
            // txtBaudRate
            // 
            this.txtBaudRate.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtBaudRate.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBaudRate.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBaudRate.Location = new System.Drawing.Point(100, 22);
            this.txtBaudRate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtBaudRate.Name = "txtBaudRate";
            this.txtBaudRate.ReadOnly = true;
            this.txtBaudRate.Size = new System.Drawing.Size(62, 15);
            this.txtBaudRate.TabIndex = 3;
            this.txtBaudRate.Text = "Baud Rate";
            // 
            // txtComPort
            // 
            this.txtComPort.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtComPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtComPort.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtComPort.Location = new System.Drawing.Point(5, 22);
            this.txtComPort.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtComPort.Name = "txtComPort";
            this.txtComPort.ReadOnly = true;
            this.txtComPort.Size = new System.Drawing.Size(63, 15);
            this.txtComPort.TabIndex = 2;
            this.txtComPort.Text = "COM Port";
            // 
            // cboBaudRate
            // 
            this.cboBaudRate.AutoCompleteCustomSource.AddRange(new string[] {
            "9600",
            "19200",
            "38400",
            "56800",
            "115200"});
            this.cboBaudRate.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBaudRate.FormattingEnabled = true;
            this.cboBaudRate.Items.AddRange(new object[] {
            "9600",
            "19200",
            "38400",
            "56800",
            "115200"});
            this.cboBaudRate.Location = new System.Drawing.Point(100, 39);
            this.cboBaudRate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cboBaudRate.Name = "cboBaudRate";
            this.cboBaudRate.Size = new System.Drawing.Size(92, 22);
            this.cboBaudRate.TabIndex = 1;
            // 
            // rtbIncomingData
            // 
            this.rtbIncomingData.Font = new System.Drawing.Font("Consolas", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbIncomingData.Location = new System.Drawing.Point(9, 413);
            this.rtbIncomingData.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rtbIncomingData.Name = "rtbIncomingData";
            this.rtbIncomingData.ReadOnly = true;
            this.rtbIncomingData.Size = new System.Drawing.Size(546, 250);
            this.rtbIncomingData.TabIndex = 2;
            this.rtbIncomingData.Text = "";
            // 
            // txtRxDataWindow
            // 
            this.txtRxDataWindow.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtRxDataWindow.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtRxDataWindow.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRxDataWindow.Location = new System.Drawing.Point(14, 398);
            this.txtRxDataWindow.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtRxDataWindow.Name = "txtRxDataWindow";
            this.txtRxDataWindow.ReadOnly = true;
            this.txtRxDataWindow.Size = new System.Drawing.Size(77, 15);
            this.txtRxDataWindow.TabIndex = 3;
            this.txtRxDataWindow.Text = "Received Data";
            // 
            // btnClearTextBox
            // 
            this.btnClearTextBox.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearTextBox.Location = new System.Drawing.Point(96, 390);
            this.btnClearTextBox.Name = "btnClearTextBox";
            this.btnClearTextBox.Size = new System.Drawing.Size(75, 23);
            this.btnClearTextBox.TabIndex = 4;
            this.btnClearTextBox.Text = "Clear";
            this.btnClearTextBox.UseVisualStyleBackColor = true;
            this.btnClearTextBox.Click += new System.EventHandler(this.btnClearTextBox_Click);
            // 
            // grpWiFiSetup
            // 
            this.grpWiFiSetup.Controls.Add(this.btnClearWifiSettings);
            this.grpWiFiSetup.Controls.Add(this.btnGenerateWebSocketKey);
            this.grpWiFiSetup.Controls.Add(this.txtWebSocketKey);
            this.grpWiFiSetup.Controls.Add(this.txtWebSocketKeyText);
            this.grpWiFiSetup.Controls.Add(this.btnWiFiGetConfiguration);
            this.grpWiFiSetup.Controls.Add(this.txtWiFiAccessPoint);
            this.grpWiFiSetup.Controls.Add(this.btnWiFiSetConfiguration);
            this.grpWiFiSetup.Controls.Add(this.txtWebSocketChannel);
            this.grpWiFiSetup.Controls.Add(this.txtWebSocketPort);
            this.grpWiFiSetup.Controls.Add(this.txtWebSocketUrl);
            this.grpWiFiSetup.Controls.Add(this.txtServerUrl);
            this.grpWiFiSetup.Controls.Add(this.txtWiFiPassword);
            this.grpWiFiSetup.Controls.Add(this.txtWebSocketPortText);
            this.grpWiFiSetup.Controls.Add(this.txtWebSocketChannelText);
            this.grpWiFiSetup.Controls.Add(this.txtWebSocketUrlText);
            this.grpWiFiSetup.Controls.Add(this.txtWiFiServerUrlText);
            this.grpWiFiSetup.Controls.Add(this.txtWiFiPasswordText);
            this.grpWiFiSetup.Controls.Add(this.txtWiFiAccessPointText);
            this.grpWiFiSetup.Font = new System.Drawing.Font("Calibri", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpWiFiSetup.Location = new System.Drawing.Point(9, 117);
            this.grpWiFiSetup.Name = "grpWiFiSetup";
            this.grpWiFiSetup.Size = new System.Drawing.Size(270, 267);
            this.grpWiFiSetup.TabIndex = 5;
            this.grpWiFiSetup.TabStop = false;
            this.grpWiFiSetup.Text = "Wi-Fi Setup";
            // 
            // btnClearWifiSettings
            // 
            this.btnClearWifiSettings.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearWifiSettings.Location = new System.Drawing.Point(164, 211);
            this.btnClearWifiSettings.Name = "btnClearWifiSettings";
            this.btnClearWifiSettings.Size = new System.Drawing.Size(59, 23);
            this.btnClearWifiSettings.TabIndex = 22;
            this.btnClearWifiSettings.Text = "Clear";
            this.btnClearWifiSettings.UseVisualStyleBackColor = true;
            this.btnClearWifiSettings.Click += new System.EventHandler(this.btnClearWifiSettings_Click);
            // 
            // btnGenerateWebSocketKey
            // 
            this.btnGenerateWebSocketKey.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateWebSocketKey.Location = new System.Drawing.Point(7, 206);
            this.btnGenerateWebSocketKey.Name = "btnGenerateWebSocketKey";
            this.btnGenerateWebSocketKey.Size = new System.Drawing.Size(75, 44);
            this.btnGenerateWebSocketKey.TabIndex = 21;
            this.btnGenerateWebSocketKey.Text = "Generate Key";
            this.btnGenerateWebSocketKey.UseVisualStyleBackColor = true;
            this.btnGenerateWebSocketKey.Click += new System.EventHandler(this.btnGenerateWebSocketKey_Click);
            // 
            // txtWebSocketKey
            // 
            this.txtWebSocketKey.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWebSocketKey.Location = new System.Drawing.Point(109, 183);
            this.txtWebSocketKey.Name = "txtWebSocketKey";
            this.txtWebSocketKey.Size = new System.Drawing.Size(155, 22);
            this.txtWebSocketKey.TabIndex = 20;
            // 
            // txtWebSocketKeyText
            // 
            this.txtWebSocketKeyText.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtWebSocketKeyText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebSocketKeyText.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWebSocketKeyText.Location = new System.Drawing.Point(5, 186);
            this.txtWebSocketKeyText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtWebSocketKeyText.Name = "txtWebSocketKeyText";
            this.txtWebSocketKeyText.ReadOnly = true;
            this.txtWebSocketKeyText.Size = new System.Drawing.Size(105, 15);
            this.txtWebSocketKeyText.TabIndex = 19;
            this.txtWebSocketKeyText.Text = "WebSocket Key";
            // 
            // btnWiFiGetConfiguration
            // 
            this.btnWiFiGetConfiguration.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWiFiGetConfiguration.Location = new System.Drawing.Point(205, 238);
            this.btnWiFiGetConfiguration.Name = "btnWiFiGetConfiguration";
            this.btnWiFiGetConfiguration.Size = new System.Drawing.Size(59, 23);
            this.btnWiFiGetConfiguration.TabIndex = 18;
            this.btnWiFiGetConfiguration.Text = "Get";
            this.btnWiFiGetConfiguration.UseVisualStyleBackColor = true;
            this.btnWiFiGetConfiguration.Click += new System.EventHandler(this.btnWiFiGetConfiguration_Click);
            // 
            // txtWiFiAccessPoint
            // 
            this.txtWiFiAccessPoint.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWiFiAccessPoint.Location = new System.Drawing.Point(109, 15);
            this.txtWiFiAccessPoint.Name = "txtWiFiAccessPoint";
            this.txtWiFiAccessPoint.Size = new System.Drawing.Size(155, 22);
            this.txtWiFiAccessPoint.TabIndex = 10;
            // 
            // btnWiFiSetConfiguration
            // 
            this.btnWiFiSetConfiguration.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWiFiSetConfiguration.Location = new System.Drawing.Point(124, 238);
            this.btnWiFiSetConfiguration.Name = "btnWiFiSetConfiguration";
            this.btnWiFiSetConfiguration.Size = new System.Drawing.Size(59, 23);
            this.btnWiFiSetConfiguration.TabIndex = 17;
            this.btnWiFiSetConfiguration.Text = "Set";
            this.btnWiFiSetConfiguration.UseVisualStyleBackColor = true;
            this.btnWiFiSetConfiguration.Click += new System.EventHandler(this.btnWiFiSetConfiguration_Click);
            // 
            // txtWebSocketChannel
            // 
            this.txtWebSocketChannel.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWebSocketChannel.Location = new System.Drawing.Point(109, 155);
            this.txtWebSocketChannel.Name = "txtWebSocketChannel";
            this.txtWebSocketChannel.Size = new System.Drawing.Size(155, 22);
            this.txtWebSocketChannel.TabIndex = 15;
            // 
            // txtWebSocketPort
            // 
            this.txtWebSocketPort.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWebSocketPort.Location = new System.Drawing.Point(109, 127);
            this.txtWebSocketPort.Name = "txtWebSocketPort";
            this.txtWebSocketPort.Size = new System.Drawing.Size(155, 22);
            this.txtWebSocketPort.TabIndex = 14;
            // 
            // txtWebSocketUrl
            // 
            this.txtWebSocketUrl.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWebSocketUrl.Location = new System.Drawing.Point(109, 99);
            this.txtWebSocketUrl.Name = "txtWebSocketUrl";
            this.txtWebSocketUrl.Size = new System.Drawing.Size(155, 22);
            this.txtWebSocketUrl.TabIndex = 13;
            // 
            // txtServerUrl
            // 
            this.txtServerUrl.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServerUrl.Location = new System.Drawing.Point(109, 71);
            this.txtServerUrl.Name = "txtServerUrl";
            this.txtServerUrl.Size = new System.Drawing.Size(155, 22);
            this.txtServerUrl.TabIndex = 12;
            // 
            // txtWiFiPassword
            // 
            this.txtWiFiPassword.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWiFiPassword.Location = new System.Drawing.Point(109, 43);
            this.txtWiFiPassword.Name = "txtWiFiPassword";
            this.txtWiFiPassword.Size = new System.Drawing.Size(155, 22);
            this.txtWiFiPassword.TabIndex = 11;
            // 
            // txtWebSocketPortText
            // 
            this.txtWebSocketPortText.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtWebSocketPortText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebSocketPortText.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWebSocketPortText.Location = new System.Drawing.Point(5, 130);
            this.txtWebSocketPortText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtWebSocketPortText.Name = "txtWebSocketPortText";
            this.txtWebSocketPortText.ReadOnly = true;
            this.txtWebSocketPortText.Size = new System.Drawing.Size(91, 15);
            this.txtWebSocketPortText.TabIndex = 9;
            this.txtWebSocketPortText.Text = "WebSocket Port#";
            // 
            // txtWebSocketChannelText
            // 
            this.txtWebSocketChannelText.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtWebSocketChannelText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebSocketChannelText.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWebSocketChannelText.Location = new System.Drawing.Point(5, 158);
            this.txtWebSocketChannelText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtWebSocketChannelText.Name = "txtWebSocketChannelText";
            this.txtWebSocketChannelText.ReadOnly = true;
            this.txtWebSocketChannelText.Size = new System.Drawing.Size(105, 15);
            this.txtWebSocketChannelText.TabIndex = 7;
            this.txtWebSocketChannelText.Text = "WebSocket Channel";
            // 
            // txtWebSocketUrlText
            // 
            this.txtWebSocketUrlText.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtWebSocketUrlText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWebSocketUrlText.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWebSocketUrlText.Location = new System.Drawing.Point(5, 102);
            this.txtWebSocketUrlText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtWebSocketUrlText.Name = "txtWebSocketUrlText";
            this.txtWebSocketUrlText.ReadOnly = true;
            this.txtWebSocketUrlText.Size = new System.Drawing.Size(86, 15);
            this.txtWebSocketUrlText.TabIndex = 6;
            this.txtWebSocketUrlText.Text = "WebSocket URL";
            // 
            // txtWiFiServerUrlText
            // 
            this.txtWiFiServerUrlText.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtWiFiServerUrlText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWiFiServerUrlText.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWiFiServerUrlText.Location = new System.Drawing.Point(5, 74);
            this.txtWiFiServerUrlText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtWiFiServerUrlText.Name = "txtWiFiServerUrlText";
            this.txtWiFiServerUrlText.ReadOnly = true;
            this.txtWiFiServerUrlText.Size = new System.Drawing.Size(63, 15);
            this.txtWiFiServerUrlText.TabIndex = 6;
            this.txtWiFiServerUrlText.Text = "Server URL";
            // 
            // txtWiFiPasswordText
            // 
            this.txtWiFiPasswordText.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtWiFiPasswordText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWiFiPasswordText.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWiFiPasswordText.Location = new System.Drawing.Point(5, 46);
            this.txtWiFiPasswordText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtWiFiPasswordText.Name = "txtWiFiPasswordText";
            this.txtWiFiPasswordText.ReadOnly = true;
            this.txtWiFiPasswordText.Size = new System.Drawing.Size(63, 15);
            this.txtWiFiPasswordText.TabIndex = 6;
            this.txtWiFiPasswordText.Text = "Password";
            // 
            // txtWiFiAccessPointText
            // 
            this.txtWiFiAccessPointText.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtWiFiAccessPointText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtWiFiAccessPointText.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWiFiAccessPointText.Location = new System.Drawing.Point(5, 18);
            this.txtWiFiAccessPointText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtWiFiAccessPointText.Name = "txtWiFiAccessPointText";
            this.txtWiFiAccessPointText.ReadOnly = true;
            this.txtWiFiAccessPointText.Size = new System.Drawing.Size(63, 15);
            this.txtWiFiAccessPointText.TabIndex = 5;
            this.txtWiFiAccessPointText.Text = "Access Point";
            // 
            // txtChargerIdText_1
            // 
            this.txtChargerIdText_1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtChargerIdText_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtChargerIdText_1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChargerIdText_1.Location = new System.Drawing.Point(5, 18);
            this.txtChargerIdText_1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtChargerIdText_1.Name = "txtChargerIdText_1";
            this.txtChargerIdText_1.ReadOnly = true;
            this.txtChargerIdText_1.Size = new System.Drawing.Size(77, 15);
            this.txtChargerIdText_1.TabIndex = 8;
            this.txtChargerIdText_1.Text = "Charger ID 1";
            // 
            // txtChargerId_1
            // 
            this.txtChargerId_1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChargerId_1.Location = new System.Drawing.Point(109, 15);
            this.txtChargerId_1.Name = "txtChargerId_1";
            this.txtChargerId_1.Size = new System.Drawing.Size(155, 22);
            this.txtChargerId_1.TabIndex = 16;
            // 
            // grpChargeStationConfig
            // 
            this.grpChargeStationConfig.Controls.Add(this.cboChargerIdPorts_1);
            this.grpChargeStationConfig.Controls.Add(this.txtChargerIdPortsText_1);
            this.grpChargeStationConfig.Controls.Add(this.btnChargeStationSetConfig);
            this.grpChargeStationConfig.Controls.Add(this.btnChargeStationGetConfig);
            this.grpChargeStationConfig.Controls.Add(this.txtChargerId_1);
            this.grpChargeStationConfig.Controls.Add(this.txtChargerIdText_1);
            this.grpChargeStationConfig.Font = new System.Drawing.Font("Calibri", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpChargeStationConfig.Location = new System.Drawing.Point(285, 118);
            this.grpChargeStationConfig.Name = "grpChargeStationConfig";
            this.grpChargeStationConfig.Size = new System.Drawing.Size(270, 266);
            this.grpChargeStationConfig.TabIndex = 6;
            this.grpChargeStationConfig.TabStop = false;
            this.grpChargeStationConfig.Text = "Charge Station Config";
            // 
            // cboChargerIdPorts_1
            // 
            this.cboChargerIdPorts_1.AutoCompleteCustomSource.AddRange(new string[] {
            "1",
            "2"});
            this.cboChargerIdPorts_1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboChargerIdPorts_1.FormattingEnabled = true;
            this.cboChargerIdPorts_1.Items.AddRange(new object[] {
            "1",
            "2"});
            this.cboChargerIdPorts_1.Location = new System.Drawing.Point(109, 40);
            this.cboChargerIdPorts_1.Name = "cboChargerIdPorts_1";
            this.cboChargerIdPorts_1.Size = new System.Drawing.Size(155, 22);
            this.cboChargerIdPorts_1.TabIndex = 21;
            this.cboChargerIdPorts_1.Text = "2";
            // 
            // txtChargerIdPortsText_1
            // 
            this.txtChargerIdPortsText_1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtChargerIdPortsText_1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtChargerIdPortsText_1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChargerIdPortsText_1.Location = new System.Drawing.Point(5, 43);
            this.txtChargerIdPortsText_1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtChargerIdPortsText_1.Name = "txtChargerIdPortsText_1";
            this.txtChargerIdPortsText_1.ReadOnly = true;
            this.txtChargerIdPortsText_1.Size = new System.Drawing.Size(77, 15);
            this.txtChargerIdPortsText_1.TabIndex = 20;
            this.txtChargerIdPortsText_1.Text = "# of Ports";
            // 
            // btnChargeStationSetConfig
            // 
            this.btnChargeStationSetConfig.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChargeStationSetConfig.Location = new System.Drawing.Point(123, 237);
            this.btnChargeStationSetConfig.Name = "btnChargeStationSetConfig";
            this.btnChargeStationSetConfig.Size = new System.Drawing.Size(59, 23);
            this.btnChargeStationSetConfig.TabIndex = 19;
            this.btnChargeStationSetConfig.Text = "Set";
            this.btnChargeStationSetConfig.UseVisualStyleBackColor = true;
            this.btnChargeStationSetConfig.Click += new System.EventHandler(this.btnChargeStationSetConfig_Click);
            // 
            // btnChargeStationGetConfig
            // 
            this.btnChargeStationGetConfig.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChargeStationGetConfig.Location = new System.Drawing.Point(208, 237);
            this.btnChargeStationGetConfig.Name = "btnChargeStationGetConfig";
            this.btnChargeStationGetConfig.Size = new System.Drawing.Size(59, 23);
            this.btnChargeStationGetConfig.TabIndex = 19;
            this.btnChargeStationGetConfig.Text = "Get";
            this.btnChargeStationGetConfig.UseVisualStyleBackColor = true;
            this.btnChargeStationGetConfig.Click += new System.EventHandler(this.btnChargeStationGetConfig_Click);
            // 
            // grpBoardInfo
            // 
            this.grpBoardInfo.Controls.Add(this.btnGetBoardInfo);
            this.grpBoardInfo.Controls.Add(this.txtFirmwareVersion);
            this.grpBoardInfo.Controls.Add(this.txtProductType);
            this.grpBoardInfo.Controls.Add(this.txtFirmwareVersionText);
            this.grpBoardInfo.Controls.Add(this.txtProductTypeText);
            this.grpBoardInfo.Font = new System.Drawing.Font("Calibri", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoardInfo.Location = new System.Drawing.Point(354, 10);
            this.grpBoardInfo.Name = "grpBoardInfo";
            this.grpBoardInfo.Size = new System.Drawing.Size(200, 102);
            this.grpBoardInfo.TabIndex = 7;
            this.grpBoardInfo.TabStop = false;
            this.grpBoardInfo.Text = "Board Info";
            // 
            // btnGetBoardInfo
            // 
            this.btnGetBoardInfo.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGetBoardInfo.Location = new System.Drawing.Point(135, 73);
            this.btnGetBoardInfo.Name = "btnGetBoardInfo";
            this.btnGetBoardInfo.Size = new System.Drawing.Size(59, 23);
            this.btnGetBoardInfo.TabIndex = 22;
            this.btnGetBoardInfo.Text = "Get";
            this.btnGetBoardInfo.UseVisualStyleBackColor = true;
            this.btnGetBoardInfo.Click += new System.EventHandler(this.btnGetBoardInfo_Click);
            // 
            // txtFirmwareVersion
            // 
            this.txtFirmwareVersion.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirmwareVersion.Location = new System.Drawing.Point(85, 46);
            this.txtFirmwareVersion.Name = "txtFirmwareVersion";
            this.txtFirmwareVersion.Size = new System.Drawing.Size(109, 22);
            this.txtFirmwareVersion.TabIndex = 25;
            // 
            // txtProductType
            // 
            this.txtProductType.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductType.Location = new System.Drawing.Point(85, 19);
            this.txtProductType.Name = "txtProductType";
            this.txtProductType.Size = new System.Drawing.Size(109, 22);
            this.txtProductType.TabIndex = 24;
            // 
            // txtFirmwareVersionText
            // 
            this.txtFirmwareVersionText.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtFirmwareVersionText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFirmwareVersionText.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFirmwareVersionText.Location = new System.Drawing.Point(5, 49);
            this.txtFirmwareVersionText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtFirmwareVersionText.Name = "txtFirmwareVersionText";
            this.txtFirmwareVersionText.ReadOnly = true;
            this.txtFirmwareVersionText.Size = new System.Drawing.Size(60, 15);
            this.txtFirmwareVersionText.TabIndex = 23;
            this.txtFirmwareVersionText.Text = "FW Version";
            // 
            // txtProductTypeText
            // 
            this.txtProductTypeText.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.txtProductTypeText.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtProductTypeText.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProductTypeText.Location = new System.Drawing.Point(5, 22);
            this.txtProductTypeText.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtProductTypeText.Name = "txtProductTypeText";
            this.txtProductTypeText.ReadOnly = true;
            this.txtProductTypeText.Size = new System.Drawing.Size(60, 15);
            this.txtProductTypeText.TabIndex = 22;
            this.txtProductTypeText.Text = "Product";
            // 
            // ConfigAppMainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(564, 674);
            this.Controls.Add(this.grpBoardInfo);
            this.Controls.Add(this.grpChargeStationConfig);
            this.Controls.Add(this.grpWiFiSetup);
            this.Controls.Add(this.btnClearTextBox);
            this.Controls.Add(this.txtRxDataWindow);
            this.Controls.Add(this.rtbIncomingData);
            this.Controls.Add(this.grpSerialPortSetup);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "ConfigAppMainWindow";
            this.Text = "JetCharge Configuration Utility";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigAppMainWindow_FormClosing);
            this.Load += new System.EventHandler(this.ConfigAppMainWindow_Load);
            this.grpSerialPortSetup.ResumeLayout(false);
            this.grpSerialPortSetup.PerformLayout();
            this.grpWiFiSetup.ResumeLayout(false);
            this.grpWiFiSetup.PerformLayout();
            this.grpChargeStationConfig.ResumeLayout(false);
            this.grpChargeStationConfig.PerformLayout();
            this.grpBoardInfo.ResumeLayout(false);
            this.grpBoardInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.ComboBox cboPorts;
        private System.Windows.Forms.GroupBox grpSerialPortSetup;
        private System.Windows.Forms.ComboBox cboBaudRate;
        private System.Windows.Forms.TextBox txtBaudRate;
        private System.Windows.Forms.TextBox txtComPort;
        private System.Windows.Forms.Button btnOpenSerialPort;
        private System.Windows.Forms.RichTextBox rtbIncomingData;
        private System.Windows.Forms.TextBox txtRxDataWindow;
        private System.Windows.Forms.Button btnClearTextBox;
        private System.Windows.Forms.GroupBox grpWiFiSetup;
        private System.Windows.Forms.TextBox txtWiFiAccessPoint;
        private System.Windows.Forms.TextBox txtWebSocketPortText;
        private System.Windows.Forms.TextBox txtChargerIdText_1;
        private System.Windows.Forms.TextBox txtWebSocketChannelText;
        private System.Windows.Forms.TextBox txtWebSocketUrlText;
        private System.Windows.Forms.TextBox txtWiFiServerUrlText;
        private System.Windows.Forms.TextBox txtWiFiPasswordText;
        private System.Windows.Forms.TextBox txtWiFiAccessPointText;
        private System.Windows.Forms.TextBox txtChargerId_1;
        private System.Windows.Forms.TextBox txtWebSocketChannel;
        private System.Windows.Forms.TextBox txtWebSocketPort;
        private System.Windows.Forms.TextBox txtWebSocketUrl;
        private System.Windows.Forms.TextBox txtServerUrl;
        private System.Windows.Forms.TextBox txtWiFiPassword;
        private System.Windows.Forms.Button btnWiFiGetConfiguration;
        private System.Windows.Forms.Button btnWiFiSetConfiguration;
        private System.Windows.Forms.Button btnGenerateWebSocketKey;
        private System.Windows.Forms.TextBox txtWebSocketKey;
        private System.Windows.Forms.TextBox txtWebSocketKeyText;
        private System.Windows.Forms.GroupBox grpChargeStationConfig;
        private System.Windows.Forms.Button btnChargeStationSetConfig;
        private System.Windows.Forms.Button btnChargeStationGetConfig;
        private System.Windows.Forms.ComboBox cboChargerIdPorts_1;
        private System.Windows.Forms.TextBox txtChargerIdPortsText_1;
        private System.Windows.Forms.GroupBox grpBoardInfo;
        private System.Windows.Forms.Button btnGetBoardInfo;
        private System.Windows.Forms.TextBox txtFirmwareVersion;
        private System.Windows.Forms.TextBox txtProductType;
        private System.Windows.Forms.TextBox txtFirmwareVersionText;
        private System.Windows.Forms.TextBox txtProductTypeText;
        private System.Windows.Forms.Button btnClearWifiSettings;
    }
}

