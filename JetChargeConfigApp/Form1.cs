﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO.Ports;

namespace JetChargeConfigApp
{
    public partial class ConfigAppMainWindow : Form
    {
        delegate void SetTextCallback(string text);
        string SerialRxData = String.Empty;

        /* Declare some Flags */
        bool F_RequestedBoardInfo = false;
        bool F_RequestedWiFiInfo = false;
        bool F_RequestedChargeStationInfo = false;

        public ConfigAppMainWindow()
        {
            InitializeComponent();
        }

        private void ConfigAppMainWindow_Load(object sender, EventArgs e)
        {
            cboPorts.Items.AddRange(SerialPort.GetPortNames());
            serialPort.Close();

            grpSerialPortSetup.BackColor = Color.LightSalmon;
            txtComPort.BackColor = Color.LightSalmon;
            txtBaudRate.BackColor = Color.LightSalmon;
            cboBaudRate.Text = "115200";

            txtFirmwareVersion.TextAlign = HorizontalAlignment.Right;
            txtProductType.TextAlign = HorizontalAlignment.Right;
        }

        private void cboPorts_DropDown(object sender, EventArgs e)
        {
            cboPorts.Items.Clear();
            cboPorts.Items.AddRange(SerialPort.GetPortNames());
         }

        /* Serial Port Event Handler */
        private void ComPortDataReceive(object sender, SerialDataReceivedEventArgs e)
        {
            SerialRxData = serialPort.ReadExisting();
            if (SerialRxData != String.Empty)
            {
                this.BeginInvoke(new SetTextCallback(SetText), new object[] { SerialRxData });
            }
        }
        /* Update Rich Text Box */
        private void SetText(string text)
        {
            /* Print Rx Data */
            this.rtbIncomingData.Text += text;
            /* Set the current text box position to the end */
            rtbIncomingData.SelectionStart = rtbIncomingData.Text.Length;
            /* Scroll Automatically */
            rtbIncomingData.ScrollToCaret();

            /* Read Rx Packet for requested Info */
            if (F_RequestedBoardInfo == true)
            {
                string[] ActualWords = new string[20];
                int j = 0;

                char[] delimiterChars = { '{', '\"', ':', '\\', '}', ',' };
                string[] words = text.Split(delimiterChars);

                for (int i = 0; i < words.Length; i++)
                {
                    if ((words[i] == "") || (words[i] == null) || words[i].Contains("\r"))
                    { }
                    else
                    {
                        ActualWords[j] = words[i];
                        if (j >= 19)
                        {
                            j = 19;
                        }
                        else
                        {
                            j++;
                        }
                    }
                }

                /* Now print the result to the Window */
                txtProductType.Text = ActualWords[7];
                txtFirmwareVersion.Text = ActualWords[9];
                /* Flush Rx Buffer */
                serialPort.DiscardInBuffer();
                F_RequestedBoardInfo = false;
            }
            else if (F_RequestedWiFiInfo == true)
            {
                string[] ActualWords = new string[20];
                int j = 0;

                char[] delimiterChars = { '{', '\"', ':', '\\', '}', ',' };
                string[] words = text.Split(delimiterChars);

                for (int i = 0; i < words.Length; i++)
                {
                    if ((words[i] == "") || (words[i] == null) || words[i].Contains("\r"))
                    { }
                    else
                    {
                        ActualWords[j] = words[i];
                        if (j >= 19)
                        {
                            j = 19;
                        }
                        else
                        {
                            j++;
                        }
                    }
                }

                /* Now print the result to the Window */
                txtWiFiAccessPoint.Text = ActualWords[7];
                txtWiFiPassword.Text = ActualWords[9];
                txtServerUrl.Text = ActualWords[11];
                txtWebSocketUrl.Text = ActualWords[13];
                txtWebSocketPort.Text = ActualWords[15];
                txtWebSocketChannel.Text = ActualWords[17];
                /* Flush Rx Buffer */
                serialPort.DiscardInBuffer();
                F_RequestedWiFiInfo = false;
            }
            else if (F_RequestedChargeStationInfo == true)
            {
                string[] ActualWords = new string[20];
                int j = 0;

                char[] delimiterChars = { '{', '\"', ':', '\\', '}', ',' };
                string[] words = text.Split(delimiterChars);

                for (int i = 0; i < words.Length; i++)
                {
                    if ((words[i] == "") || (words[i] == null) || words[i].Contains("\r"))
                    { }
                    else
                    {
                        ActualWords[j] = words[i];
                        if (j >= 19)
                        {
                            j = 19;
                        }
                        else
                        {
                            j++;
                        }
                    }
                }

                /* Now print the result to the Window */
                txtChargerId_1.Text = ActualWords[7] + ':' + ActualWords[8] + ':' + ActualWords[9] + ':' +
                                      ActualWords[10] + ':' + ActualWords[11] + ':' + ActualWords[12];
                cboChargerIdPorts_1.Text = ActualWords[13];
 
                /* Flush Rx Buffer */
                serialPort.DiscardInBuffer();
                F_RequestedChargeStationInfo = false;
            }

        }

        private void btnClearTextBox_Click(object sender, EventArgs e)
        {
            rtbIncomingData.Clear();
        }

        private void btnOpenSerialPort_Click(object sender, EventArgs e)
        {
            if (btnOpenSerialPort.Text == "Open Port")
            {
                /* Setup the Serial Port Parameters and open the Port */
                serialPort.PortName = cboPorts.Text;
                serialPort.BaudRate = 115200;// Convert.ToInt32(cboBaudRate.Text);
                serialPort.DataBits = 8;
                serialPort.StopBits = StopBits.One;
                serialPort.Parity = Parity.None;
                serialPort.Handshake = Handshake.None;

                if (serialPort.IsOpen)
                {
                    MessageBox.Show("Port is already Open", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    try
                    {
                        serialPort.Open();
                        /* Do Aestethics */
                        grpSerialPortSetup.BackColor = Color.LightGreen;
                        txtComPort.BackColor = Color.LightGreen;
                        txtBaudRate.BackColor = Color.LightGreen;
                        btnOpenSerialPort.Text = "Close Port";
                    }
                    catch (UnauthorizedAccessException ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else if (btnOpenSerialPort.Text == "Close Port")
            {
                /* Close the Serial Port */
                serialPort.Close();
                grpSerialPortSetup.BackColor = Color.LightSalmon;
                txtComPort.BackColor = Color.LightSalmon;
                txtBaudRate.BackColor = Color.LightSalmon;
                btnOpenSerialPort.Text = "Open Port";
            }
        }

        private void ConfigAppMainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            serialPort.Close();
        }

        private void btnGenerateWebSocketKey_Click(object sender, EventArgs e)
        {
            Guid guid = Guid.NewGuid();
            byte[] bytes = guid.ToByteArray();
            string encodedString = Convert.ToBase64String(bytes);
            /* Get rid of some unwanted characters */
            encodedString = encodedString.Replace("\\","a");
            encodedString = encodedString.Replace("/", "b");
            encodedString = encodedString.Replace("+", "c");

            txtWebSocketKey.Text = encodedString;
        }

        private void btnGetBoardInfo_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("{\"command\":\"get\",\"identifier\":\"{\\\"config\\\":\\\"boardinfo\\\"}\",\"data\":\"{}}\r\n");
                F_RequestedBoardInfo = true;
            }
            else
            {
                MessageBox.Show("Serial port not open!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClearWifiSettings_Click(object sender, EventArgs e)
        {
            txtWiFiAccessPoint.Text = String.Empty;
            txtWiFiPassword.Text = String.Empty;
            txtServerUrl.Text = String.Empty;
            txtWebSocketUrl.Text = String.Empty;
            txtWebSocketPort.Text = String.Empty;
            txtWebSocketChannel.Text = String.Empty;
        }

        private void btnChargeStationGetConfig_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("{\"command\":\"get\",\"identifier\":\"{\\\"config\\\":\\\"chargestation\\\"}\",\"data\":\"{}}\r\n");
                F_RequestedChargeStationInfo = true;
            }
            else
            {
                MessageBox.Show("Serial port not open!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnWiFiGetConfiguration_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                serialPort.Write("{\"command\":\"get\",\"identifier\":\"{\\\"config\\\":\\\"wifi\\\"}\",\"data\":\"{}}\r\n");
                F_RequestedWiFiInfo = true;
            }
            else
            {
                MessageBox.Show("Serial port not open!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnWiFiSetConfiguration_Click(object sender, EventArgs e)
        {
            string stringToSend = "";

            if (serialPort.IsOpen)
            {
                if (txtWiFiAccessPoint.Text == string.Empty)
                {
                    MessageBox.Show("Please Enter Wi-Fi Access Point to connect to", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (txtWiFiPassword.Text == string.Empty)
                {
                    MessageBox.Show("Please enter the Wi-Fi password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (txtServerUrl.Text == string.Empty)
                {
                    MessageBox.Show("Please specify the Server URL/IP Address", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (txtWebSocketUrl.Text == string.Empty)
                {
                    MessageBox.Show("Please Specifi the Websocket URL", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (txtWebSocketPort.Text == string.Empty)
                {
                    MessageBox.Show("Please specify the Server Port number to connect to", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (txtWebSocketChannel.Text == string.Empty)
                {
                    MessageBox.Show("Please specify the Websocket Channel to connect to", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (txtWebSocketKey.Text == string.Empty)
                {
                    MessageBox.Show("Please generate a Web-Socket Key", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    stringToSend = "{\"command\":\"set\",\"identifier\":\"{\\\"config\\\":\\\"wifi\\\"}\",\"data\":\"{" +
                        "\\\"wifiap\\\":\\\"" + txtWiFiAccessPoint.Text + "\\\"," +
                        "\\\"pwd\\\":\\\"" + txtWiFiPassword.Text + "\\\"," +
                        "\\\"serverurl\\\":\\\"" + txtServerUrl.Text + "\\\"," +
                        "\\\"websocketurl\\\":\\\"" + txtWebSocketUrl.Text + "\\\"," +
                        "\\\"websocketport\\\":\\\"" + txtWebSocketPort.Text + "\\\"," +
                        "\\\"websocketchannel\\\":\\\"" + txtWebSocketChannel.Text + "\\\"," +
                        "\\\"websocketkey\\\":\\\"" + txtWebSocketKey.Text + "\\\"" +
                        "}}\r\n";
                    this.rtbIncomingData.Text += stringToSend;
                    serialPort.Write(stringToSend);
                }
            }
            else
            {
                MessageBox.Show("Serial port not open!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnChargeStationSetConfig_Click(object sender, EventArgs e)
        {
            string stringToSend = "";

            if (serialPort.IsOpen)
            {
                if (txtChargerId_1.Text == string.Empty)
                {
                    MessageBox.Show("Please Enter Charge Station Name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (cboChargerIdPorts_1.Text == string.Empty)
                {
                    MessageBox.Show("Please select number of Ports for Charge Station", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    stringToSend = "{\"command\":\"set\",\"identifier\":\"{\\\"config\\\":\\\"charger\\\"}\",\"data\":\"{" +
                        "\\\"name1\\\":\\\"" + txtChargerId_1.Text + "\\\"," +
                        "\\\"ports1\\\":\\\"" + cboChargerIdPorts_1.Text + "\\\"," +
                        "}}\r\n";
                    this.rtbIncomingData.Text += stringToSend;
                    serialPort.Write(stringToSend);
                }
            }
            else
            {
                MessageBox.Show("Serial port not open!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
